
Experiments conducted by varying the following hyper-parameters
- Truncated backpropagation
- Window sizes
- Type of Gradient updates
- Gradient clipping
- Epochs
- Probabilistic prediction of next character

### Initial Experiments ###

#### Hyper-parameters ####

- Train samples - 4500
- Test samples - 500
- Learning rate - 0.003
- Hidden units -  100
- Epochs - 100, 200, 500
- Optimizer - Adam

![alt text](initial_exp.jpg)
![alt text](initial_names.jpg)


#### Observations ####

- Lower the perplexity, better the model.
- End of window updates is observed to perform better than end of sequence. 
- The training epochs of 500 is observed to provide better perplexity metric.
- The RNN model which includes the upper-case characters for classification learns the probability distribution of characters better.
- The window size of 3 works better in terms of lower perplexity.



### Length of name encoding Experiment ###

#### Hyper-parameters ####
- Train samples - 4500
- Generation samples - 2505
- Learning rate - 0.003
- Hidden units -  100
- Epochs - 250, 500
- Optimizer - Adam
- Update Type - Window

![alt text](length_exp.jpg)
![alt text](length_compare.jpg)
![alt text](final_names.jpg)

#### Observations ####

- The length of name encoded with every character of input works very well compared to other models.
- The name gen model learns to generate names of specific length.
- As the window size increases, the length rule is obeyed well.
- Window size 3 and 250 epochs works the best compared to other configs
- Two generation approaches 
      - Provide with # (start) character for generation - room for more unique names
      - Provide with # (start) and the first alphabet for generation
- Both the generation approaches provides names with same accuracy of the length constraint.
- The names generated of various lengths are unique and reasonable. 
- End of window updates provides lesser perplexity than end of sequence updates.
