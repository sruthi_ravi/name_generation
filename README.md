# README #

### Baby name generation using char-RNN
This repository has a working model of character based RNN architecture experimented on baby name generation. The network is trained using sequence of characters that represents a baby name. At each time instance, a character is fed as input to the RNN and next character is predicted as output.
During the generation phase, the network generates new baby names.
We also experimented on encoding the length of the name along with the names during training so that the network learns to generate a name of specific length.

### Encoding length of the name into the data ###
Train the RNN with ..
* The length of name encoded with every character of input.
* The length of name given as the first character of input.
* The decreasing length of the name encoded with every character of input.


### DataSet ###

* The dataset contains set of baby names organized as 'male.txt' and 'female.txt' (/data/source). 
* The raw data is preprocessed and indexed for each character. The following folders contain preprocessed data -
    - /data/gen/caps - contains indexed vector of names for uppercase and lowercase characters.
    - /data/gen/not_caps - contains indexed vector of names after the names are converted to lowercase characters.
    - /data/gen/startlen - contains indexed vector of names along with the length of the name provided as the starting character.
    Every name is preceeded(#) and succeeded(.) by start and end character.
* Each of the above directories have train and test files.
* Train/Test split
    - Train - 4500 names
    - Train full data - 5000 names
    - Generated - 2500 names
 

#### Data Sample ####
* /data/gen/caps
    - Sample X: #Jane
    - Sample Y: Jane.

* /data/gen/not_caps
    - Sample X: #jane
    - Sample Y: jane.

* /data/gen/startlen
    - Sample X: 4jane
    - Sample Y: jane.

### How do I set up? ###

* Model set up
    - /core contains the files for the char-RNN model architecture.
    - /core/activation - Forward and backward logic for activation functions 
    - /core/gates - RNN gate operations
    - /core/layer - Layer architecture
    - /core/model - Model architecture
* Experiment set up
    /experiments contains the train and test set up files
    /experiments/character_rnn_test - Helper files for model initialization and configuration.
    - init_model function - defines the configuration such as input, hidden and output dimension and the hyper-parameters for char-RNN.
    - The helper file also defines the location of the data and parameters weights to save the weights after training. The same paths are used during test time.  
    - /experiments/character_rnn_train - Loads the data and invoke the model training.
    - /experiments/character_rnn_test - Loads the data and invoke the model prediction.
    - /experiment_rnn - Set of different experiments by changing hyper-parameters conducted on RNN. Supports automated training by adding required configs to the training chain and automatically saves the parameters for each hyper-parameter setting to the path specified in the configs.
    This file is starting point for training.
* Output and Parameters
    - /output - saves the generated baby names files in this directory
    - /parameters - saves the model parameters weight matrices (u,v,w) files in this directory
    - The details of the various experiment outputs is present in /output/README.md
    
* Configurable parameters
    * Epochs
    * Learning rate
    * Input, hidden and output dimension
    * bptt truncate size
    * Optimizer - 'adam'. If nothing is specified default is gradient descent.
    * Parameter update type - 'sequence' or 'window' for end of sequence or window updates.
    * Output activation function 

* How to run
    - To start training, add the necessary configs in the experiments/experiment_rnn file and run the file.
    - To start generation, run the /experiments/character_rnn_test file after changing the saved parameters path in /experiments/character_rnn_helper file.
    - The perplexity metric for each experiment is written at the end of the generated file stored in /output.