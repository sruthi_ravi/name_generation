from experiments import character_rnn_helper as helper
import numpy as np
import random

a_to_i = helper.char_rnn_encode()
i_to_a = helper.char_rnn_decode()
parameter_file = 'config_4_best_caps_w5_500_len_bit'


def predict_span(x_test, y_test):
    u = np.load(helper.output_param_dir + parameter_file + '_u.npy')
    v = np.load(helper.output_param_dir + parameter_file + '_v.npy')
    w = np.load(helper.output_param_dir + parameter_file + '_w.npy')

    rnn = helper.init_model({}, u, v, w)
    with open(helper.output_file, 'w') as fw:
        span = 2
        for i in range(len(x_test)):
            for t in range(5):
                first_char = x_test[i][0:span]
                output = ''.join(i_to_a[c] for c in first_char)
                s = None
                #l = len(x_test[i]) - 1
                while first_char[-1] != 27:
                    next_char, prev_s = rnn.predict_span(first_char, len(x_test[i])-1, s=s)  # for the start (#) character
                    #next_char, prev_s = rnn.predict_span(first_char, l, s=s)
                    #output += ''.join(i_to_a[c] for c in next_char)
                    output += ''.join(i_to_a[next_char[-1]])
                    first_char = [next_char[-1]]
                    s = prev_s[-1]
                    #l-=1 if l !=0 else l
                    #l = l - 2 if l == len(x_test[i]) - 1 else l - 1
                    #l = 0 if l < 0 else l
                fw.write('{}\t{}\t{}\n'.format(output[1:], len(output) - 2, len(x_test[i]) - 1))
        fw.write('{}\n'.format(calculate_perplexity(x_test, y_test)))
    fw.close()


def calculate_perplexity(x_test, y_test):
    u = np.load(helper.output_param_dir + parameter_file + '_u.npy')
    v = np.load(helper.output_param_dir + parameter_file + '_v.npy')
    w = np.load(helper.output_param_dir + parameter_file + '_w.npy')
    M = len(x_test)
    N = len(x_test[0])
    num = M * N
    rnn = helper.init_model({}, u, v, w)
    sum = 0
    for i in range(len(x_test)):
        input = []
        for j in range(len(x_test[i])):
            input.append(x_test[i][j])
            #input = [x_test[i][j]]
            probability = rnn.perplexity(input, y_test[i][j], len(x_test[i]) - 1)[-1]
            sum += np.log(probability)
    metric = np.exp(-sum / num)
    print(metric)
    return metric


def predict(x_test, y_test):
    # read the parameters from the numpy file and do the forward propagation
    u = np.load(helper.parameter_file['u'])
    v = np.load(helper.parameter_file['v'])
    w = np.load(helper.parameter_file['w'])

    rnn = helper.init_model({}, u, v, w)
    with open(helper.output_file, 'w') as fw:
        for i in range(len(x_test)):
            output = rnn.predict(x_test[i][1:])
            fw.write('{}\n'.format(output))
        fw.write('{}\n'.format(calculate_perplexity(x_test, y_test)))
    fw.close()


def main():
    data_file = helper.data_file
    x_test = np.load(data_file['x_test'])
    y_test = np.load(data_file['y_test'])

    # predict(x_test, y_test)
    predict_span(x_test, y_test)
    calculate_perplexity(x_test, y_test)


if __name__ == "__main__":
    main()
