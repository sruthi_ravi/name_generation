import random
import numpy as np
from core.model import Model
from core.activation import Softmax
from core.activation import Sigmoid
from core.activation import SigmoidwithCrossEntropy

input_file = '../data/source/female.txt'
output_param_dir = '../parameters/'
output_file = '../output/config_4_best_caps_w5_500_len_bit_1.txt'
parameter_file = 'config_4_best_caps_w5_500_len_bit'

parameter_file = {
    'u': '../parameters/sample_u.npy',

    'v': '../parameters/sample_v.npy',
    'w': '../parameters/sample_w.npy'
}

data_file_sorted = {
    'x_train': '../data/gen/caps/sorted/x_train_6.npy',
    'y_train': '../data/gen/caps/sorted/y_train_6.npy',
    'x_test': '../data/gen/caps/sorted/x_test_6.npy',
    'y_test': '../data/gen/caps/sorted/y_test_6.npy'
}

data_file_caps = {
    'x_train': '../data/gen/caps/x_train.npy',
    'y_train': '../data/gen/caps/y_train.npy',
    'x_test': '../data/gen/caps/x_test.npy',
    'y_test': '../data/gen/caps/y_test.npy'
}

data_file_startlen = {
    'x_train': '../data/gen/startlen/x_train.npy',
    'y_train': '../data/gen/startlen/y_train.npy',
    'x_test': '../data/gen/startlen/x_test.npy',
    'y_test': '../data/gen/startlen/y_test.npy'
}

data_file = data_file_caps


def word_vector(z, size, len=None):
    input = np.zeros(size)
    input[z] = 1
    return input
    # binary = '{0:05b}'.format(z)
    # return np.array([int(val) for val in binary])


def word_vector_len(z, size, len=None):
    input = np.zeros(size)
    input[z] = 1
    if len is not None:
        input[char_rnn_encode()[str(len)]] = 1
    return input


def init_model(params={}, u=None, v=None, w=None):
    word_dim = 68
    hidden_dim = 100
    output_dim = 54
    learning_rate = params['learning_rate'] if params and 'learning_rate' in params else 1e-3
    optimizer = params['optimizer'] if params and 'optimizer' in params else None
    update_type = params['update_type'] if params and 'update_type' in params else None
    window_size = params['window_size'] if params and 'window_size' in params else 0
    return Model(word_dim, output_dim, Softmax, word_vector_len, hidden_dim, window_size, learning_rate=learning_rate,
                 optimizer=optimizer, update_type=update_type, u=u, v=v, w=w)


def character_pad(text):
    text = ''.join(filter(str.isalpha, text))

    char_text = list(text)
    encoded_text = [char_rnn_encode()[c] for c in char_text]
    name_len = len(encoded_text)
    x = [char_rnn_encode()['#']] + encoded_text
    y = x[1:] + [char_rnn_encode()['.']]
    # x += [char_encode['0']] * (length - len_x)
    # y += [char_encode['0']] * (length - len_y)
    return x, y


def get_data():
    x_data = []
    y_data = []
    text = []
    with open(input_file, 'r')as f:
        for line in f:
            text.append(line.strip())
    f.close()
    random.seed(0)
    random.shuffle(text)
    for name in text:
        x, y = character_pad(name)
        x_data.append(np.array(x))
        y_data.append(np.array(y))

    x_data = np.array(x_data)
    y_data = np.array(y_data)
    x_train = x_data[:4500]
    y_train = y_data[:4500]
    x_test = x_data[4500:]
    y_test = y_data[4500:]
    np.save(data_file['x_train'], x_train)
    np.save(data_file['y_train'], y_train)
    np.save(data_file['x_test'], x_test)
    np.save(data_file['y_test'], y_test)
    return x_train, y_train, x_test, y_test


def write_output(file, data):
    with open(file, 'w') as fw:
        for val in data:
            fw.write(','.join(str(v) for v in val) + '\n')
    fw.close()


def char_rnn_encode():
    return {'a': 0, 'b': 1, 'c': 2, 'd': 3, 'e': 4, 'f': 5, 'g': 6, 'h': 7, 'i': 8, 'j': 9, 'k': 10, 'l': 11, 'm': 12,
            'n': 13, 'o': 14, 'p': 15, 'q': 16, 'r': 17, 's': 18, 't': 19, 'u': 20, 'v': 21, 'w': 22, 'x': 23, 'y': 24,
            'z': 25, '#': 26, '.': 27, 'A': 28, 'B': 29, 'C': 30, 'D': 31, 'E': 32, 'F': 33, 'G': 34, 'H': 35, 'I': 36,
            'J': 37, 'K': 38, 'L': 39, 'M': 40, 'N': 41, 'O': 42, 'P': 43, 'Q': 44, 'R': 45, 'S': 46, 'T': 47, 'U': 48,
            'V': 49, 'W': 50, 'X': 51, 'Y': 52, 'Z': 53,
            # 3
            '3': 54,
            # 4
            '4': 55,
            # 5
            '5': 56,
            # 6
            '6': 57,
            # 7
            '7': 58,
            # 8
            '8': 59,
            # 9
            '9': 60,
            # 10
            '10': 61,
            # 11
            '11': 62,
            # 12
            '12': 63,
            # 13
            '13': 64,
            # 14
            '14': 65,
            # 15
            '15': 66,
            # 2
            '2': 67,
            '1': 68,
            '0': 69
            }


def char_rnn_decode():
    return dict((v, k) for k, v in char_rnn_encode().items())


def parse_file():
    fw = open('../data/names_config_4_500_eos.txt', 'w')
    with open('../data/test_config_4_500_eos.txt', 'r') as f:
        for line in f:
            line = line.strip().replace('[', '').replace(']', '')
            name = ''.join(char_rnn_decode()[int(c)] for c in line.split(' ') if c != '')
            fw.write(name + '\n')
    fw.close()
    f.close()

# parse_file()
# get_data()
