import numpy as np
# compare how many names in the output file obey the length constraint

def asses_len():
    output_file = '../output/config_4_best_caps_w_250_len_bit.txt'
    metrics = 0
    with open(output_file, 'r') as f:
        for line in f:
            value = line.split()
            op = int(value[1])
            tv = int(value[2])
            if op == tv or op - 1 == tv or op + 1 == tv or op-2 ==tv or op+2 ==tv:
                metrics += 1

    print(metrics)

asses_len()

def split_files(x, y):
    x_t = np.load(x)
    y_t = np.load(y)
    split = dict()
    for i in range(len(x_t)):
        size = len(x_t[i])
        if size not in split:
            split[size] = list()
            split[size].append([])
            split[size].append([])
        split[size][0].append(x_t[i])
        split[size][1].append(y_t[i])

    for key, value in split.items():
        np.save('../data/gen/not_caps/sorted/x_test_'+str(key)+'.npy', np.array(value[0]))
        np.save('../data/gen/not_caps/sorted/y_test_'+str(key)+'.npy', np.array(value[1]))
x_train = '../data/gen/not_caps/x_train.npy'
y_train = '../data/gen/not_caps/y_train.npy'
x_test = '../data/gen/not_caps/x_test.npy'
y_test = '../data/gen/not_caps/y_test.npy'

#split_files(x_test, y_test)