import sys
from experiments import character_rnn_train

config_1 = {
               'optimizer': 'adam',
               'update_type': 'window',
               'window_size': 0,
               'param_file': 'config_1',
               'learning_rate': 1e-3
           },
config_2 = {
               'optimizer': 'adam',
               'update_type': 'window',
               'window_size': 3,
               'param_file': 'config_2',
               'learning_rate': 1e-3

           },
config_3 = {
               'optimizer': 'adam',
               'update_type': 'window',
               'window_size': 5,
               'param_file': 'config_3',
               'learning_rate': 1e-3

           },
config_4 = {
               'optimizer': 'adam',
               'window_size': 0,
               'param_file': 'config_4',
               'learning_rate': 1e-3,
               'update_type': 'win',

           },
config_5 = {
               'optimizer': 'adam',
               'window_size': 3,
               'param_file': 'config_5',
               'learning_rate': 1e-3,
               'update_type': 'win'
           },
config_6 = {
               'optimizer': 'adam',
               'window_size': 5,
               'param_file': 'config_6',
               'learning_rate': 1e-3,
               'update_type': 'win'
           },

config_4_200 = {
    'optimizer': 'adam',
    'window_size': 0,
    'param_file': 'config_4_200',
    'learning_rate': 1e-2,
    'update_type': 'win',
    'epoch': 200
}
config_4_300 = {
    'optimizer': 'adam',
    'window_size': 0,
    'param_file': 'config_4_300',
    'learning_rate': 1e-3,
    'update_type': 'window',
    'epoch': 300
}
config_4_400 = {
    'optimizer': 'adam',
    'window_size': 0,
    'param_file': 'config_4_400',
    'learning_rate': 1e-3,
    'update_type': 'win',
    'epoch': 400

}

config_4_500_eos = {
    'optimizer': 'adam',
    'window_size': 5,
    'param_file': 'config_4_500_eos',
    'learning_rate': 1e-3,
    'update_type': 'win',
    'epoch': 500

}
config_4_500 = {
    'optimizer': 'adam',
    'window_size': 5,
    'param_file': 'config_4_500',
    'learning_rate': 1e-3,
    'update_type': 'window',
    'epoch': 500

}

config_4_best_caps = {
                         'optimizer': 'adam',
                         'window_size': 2,
                         'param_file': 'config_4_best_caps',
                         'learning_rate': 1e-3,
                         'update_type': 'window',
                         'epoch': 200

                     },

config_4_best_caps_500 = {
                             'optimizer': 'adam',
                             'window_size': 0,
                             'param_file': 'config_4_best_caps_500',
                             'learning_rate': 1e-3,
                             'update_type': 'win',
                             'epoch': 400

                         },

config_4_best_caps_250 = {
                             'optimizer': 'adam',
                             'window_size': 2,
                             'param_file': 'config_4_best_caps_w_250',
                             'learning_rate': 1e-3,
                             'update_type': 'window',
                             'epoch': 250

                         },
config_4_best_caps_350 = {
                             'optimizer': 'adam',
                             'window_size': 2,
                             'param_file': 'config_4_best_caps_w_350',
                             'learning_rate': 1e-3,
                             'update_type': 'window',
                             'epoch': 350

                         },

config_4_best_caps_w_500 = {
                               'optimizer': 'adam',
                               'window_size': 2,
                               'param_file': 'config_4_best_caps_w_500',
                               'learning_rate': 1e-3,
                               'update_type': 'window',
                               'epoch': 500

                           },

config_4_best_caps_s_500 = {
                               'optimizer': 'adam',
                               'window_size': 2,
                               'param_file': 'config_4_best_caps_w_500',
                               'learning_rate': 1e-3,
                               'update_type': 'sequence',
                               'epoch': 500

                           },
config_4_best_caps_w_750 = {
                               'optimizer': 'adam',
                               'window_size': 2,
                               'param_file': 'config_4_best_caps_w_750',
                               'learning_rate': 1e-3,
                               'update_type': 'window',
                               'epoch': 750

                           },
config_4_best_caps_w_1000 = {
                                'optimizer': 'adam',
                                'window_size': 2,
                                'param_file': 'config_4_best_caps_w_1000',
                                'learning_rate': 1e-3,
                                'update_type': 'window',
                                'epoch': 1000

                            },

config_4_best_caps_w_500_len = {
                                   'optimizer': 'adam',
                                   'window_size': 2,
                                   'param_file': 'config_4_best_caps_w_500_len',
                                   'learning_rate': 1e-3,
                                   'update_type': 'window',
                                   'epoch': 500

                               },

config_4_best_caps_s_500_len = {
                                   'optimizer': 'adam',
                                   'window_size': 2,
                                   'param_file': 'config_4_best_caps_s_500_len',
                                   'learning_rate': 1e-3,
                                   'update_type': 'sequence',
                                   'epoch': 500

                               },

trial = {
            'optimizer': 'adam',
            'window_size': 2,
            'param_file': 'trail',
            'learning_rate': 1e-3,
            'update_type': 'window',
            'epoch': 250

        },

config_4_best_caps_w_750_len_bit = {
                                       'optimizer': 'adam',
                                       'window_size': 2,
                                       'param_file': 'config_4_best_caps_w_750_len_bit',
                                       'learning_rate': 1e-3,
                                       'update_type': 'window',
                                       'epoch': 750

                                   },

config_4_best_caps_w_500_len_decay = {
                                         'optimizer': 'adam',
                                         'window_size': 2,
                                         'param_file': 'config_4_best_caps_w_500_len_decay',
                                         'learning_rate': 1e-3,
                                         'update_type': 'window',
                                         'epoch': 500

                                     },

config_4_best_caps_s_500_len_bit = {
                                       'optimizer': 'adam',
                                       'window_size': 2,
                                       'param_file': 'config_4_best_caps_s_500_len_bit',
                                       'learning_rate': 1e-3,
                                       'update_type': 'sequence',
                                       'epoch': 500

                                   },
config_4_best_caps_w5_250_len_bit = {
                                        'optimizer': 'adam',
                                        'window_size': 4,
                                        'param_file': 'config_4_best_caps_w5_250_len_bit',
                                        'learning_rate': 1e-3,
                                        'update_type': 'window',
                                        'epoch': 250

                                    },
config_4_best_caps_w5_500_len_bit = {
                                        'optimizer': 'adam',
                                        'window_size': 4,
                                        'param_file': 'config_4_best_caps_w5_500_len_bit',
                                        'learning_rate': 1e-3,
                                        'update_type': 'window',
                                        'epoch': 500

                                    },
config_4_best_caps_w7_250_len_bit = {
                                        'optimizer': 'adam',
                                        'window_size': 6,
                                        'param_file': 'config_4_best_caps_w7_250_len_bit',
                                        'learning_rate': 1e-3,
                                        'update_type': 'window',
                                        'epoch': 250

                                    },
full_data_config_4_best_caps_w1_250_len_bit = {
                                                  'optimizer': 'adam',
                                                  'window_size': 0,
                                                  'param_file': 'full_data_config_4_best_caps_w1_250_len_bit',
                                                  'learning_rate': 1e-3,
                                                  'update_type': 'window',
                                                  'epoch': 250

                                              },

full_data_config_4_best_caps_w3_250_len_bit = {
                                                  'optimizer': 'adam',
                                                  'window_size': 2,
                                                  'param_file': 'full_data_config_4_best_caps_w3_250_len_bit',
                                                  'learning_rate': 1e-3,
                                                  'update_type': 'window',
                                                  'epoch': 250
                                              },

full_data_config_4_best_caps_w5_250_len_bit = {
                                                  'optimizer': 'adam',
                                                  'window_size': 4,
                                                  'param_file': 'full_data_config_4_best_caps_w5_250_len_bit',
                                                  'learning_rate': 1e-3,
                                                  'update_type': 'window',
                                                  'epoch': 250

                                              },
full_data_config_4_best_caps_w7_250_len_bit = {
                                                  'optimizer': 'adam',
                                                  'window_size': 6,
                                                  'param_file': 'full_data_config_4_best_caps_w7_250_len_bit',
                                                  'learning_rate': 1e-3,
                                                  'update_type': 'window',
                                                  'epoch': 250

                                              },
# best model so far compared to all experiments 8.07 perplexity
config_4_best_caps_w_250_len_bit = {
                                       'optimizer': 'adam',
                                       'window_size': 2,
                                       'param_file': 'config_4_best_caps_w_250_len_bit',
                                       'learning_rate': 1e-3,
                                       'update_type': 'window',
                                       'epoch': 250

                                   },


def experiment():
    param_config = []
    param_config.append(config_4_best_caps_w_250_len_bit)

    for config in param_config:
        character_rnn_train.train(config[0])


experiment()
