
import numpy as np
from experiments import character_rnn_helper as helper



def train(params=None):
    data_file = helper.data_file
    x_train = np.load(data_file['x_train'])
    y_train = np.load(data_file['y_train'])
    x_train = np.concatenate((x_train, np.load(data_file['x_test'])))
    y_train = np.concatenate((y_train, np.load(data_file['y_test'])))

    # u = np.load(helper.output_param_dir + parameter_file + '_u.npy')
    # v = np.load(helper.output_param_dir + parameter_file + '_v.npy')
    # w = np.load(helper.output_param_dir + parameter_file + '_w.npy')

    # init model params
    rnn = helper.init_model(params=params)
    # # train the network
    epoch = params['epoch'] if params and 'epoch' in params else 100
    rnn.train(x_train, y_train, nepoch=epoch, evaluate_loss_after=10)
    # # save the trained model params
    print('Saving model parameters....')
    np.save(helper.output_param_dir+params['param_file']+'_u.npy', rnn.U)
    np.save(helper.output_param_dir+params['param_file']+'_w.npy', rnn.W)
    np.save(helper.output_param_dir+params['param_file']+'_v.npy', rnn.V)


def main():
    train()


if __name__ == '__main__':
    main()
