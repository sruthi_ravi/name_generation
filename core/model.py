import numpy as np
from core.layer import RNNLayer as Layer


class Model:
    def __init__(self, word_dim, output_dim, output_layer, generator, hidden_dim=100, bptt_truncate=4,
                 learning_rate=1e-3, optimizer=None, update_type='window', u=None, v=None,
                 w=None):
        self.word_dim = word_dim
        self.hidden_dim = hidden_dim
        self.output_dim = output_dim
        self.output_layer = output_layer
        self.bptt_truncate = bptt_truncate
        self.generator = generator
        self.update_type = update_type
        self.learning_rate = learning_rate
        self.optimizer = self.adam_optimizer if optimizer == 'adam' else self.gd_optimizer
        self.bptt = self.bptt_window if self.update_type == 'window' else self.bptt_seq
        self.adam = {
            'mt': {},
            'vt': {},
            't': 0,
            'beta1': 0.9,
            'beta2': 0.999,
            'eps': 1e-4
        }
        np.random.seed(seed=0)
        self.U = u if u is not None else np.random.uniform(-np.sqrt(1. / word_dim), np.sqrt(1. / word_dim),
                                                           (hidden_dim, word_dim))
        self.W = w if w is not None else np.random.uniform(-np.sqrt(1. / hidden_dim), np.sqrt(1. / hidden_dim),
                                                           (hidden_dim, hidden_dim))
        self.V = v if v is not None else np.random.uniform(-np.sqrt(1. / hidden_dim), np.sqrt(1. / hidden_dim),
                                                           (output_dim, hidden_dim))
        self.orig_len = None

    '''
        forward propagation (predicting word probabilities)
        x is one single data, and a batch of data
        for example x = [0, 179, 341, 416], then its y = [179, 341, 416, 1]
    '''

    def forward_propagation(self, x, s=None):
        # The total number of time steps
        T = len(x)
        layers = []
        if s is None:
            s = np.zeros(self.hidden_dim)
            # s[T] = 1
        prev_s = s
        # For each time step...
        for t in range(T):  # 0- 4
            layer = Layer()
            input = self.generator(x[t], self.word_dim, self.orig_len)
            #input = self.generator(x[t], self.word_dim, self.orig_len - t)
            layer.forward(input, prev_s, self.U, self.W, self.V)
            prev_s = layer.s
            layers.append(layer)
        return layers

    def predict_span(self, x, gen_len=None, s=None):
        output = self.output_layer()
        self.orig_len = gen_len
        layers = self.forward_propagation(x, s=s)
        result = []
        prev_s = []
        for layer in layers:
            result.append(output.random_y(output.predict(layer.mulv)))
            prev_s.append(layer.s)
        return np.array(result), np.array(prev_s)

    def predict(self, x):
        output = self.output_layer()
        layers = self.forward_propagation(x)
        return np.array([output.get_y(output.predict(layer.mulv)) for layer in layers])

    def perplexity(self, x, index, gen_len):
        output = self.output_layer()
        self.orig_len = gen_len
        layers = self.forward_propagation(x)
        return np.array([output.get_y_at_index(output.predict(layer.mulv), index) for layer in layers])

    def calculate_loss(self, x, y):
        assert len(x) == len(y)
        output = self.output_layer()
        self.orig_len = len(x) - 1
        layers = self.forward_propagation(x)
        loss = 0.0
        for i, layer in enumerate(layers):
            loss += output.loss(layer.mulv, y[i])
        return loss / float(len(y))

    def calculate_total_loss(self, X, Y):
        loss = 0.0
        for i in range(len(Y)):
            loss += self.calculate_loss(X[i], Y[i])
        return loss / float(len(Y))

    def adam_optimizer(self, parameters):
        grads = parameters['grads']
        weights = parameters['params']
        for n, v in grads.items():
            dv = v
            self.adam['t'] += 1
            self.adam['mt'][n] = self.adam['beta1'] * self.adam['mt'].get(n, 0) + (1 - self.adam['beta1']) * dv
            self.adam['vt'][n] = self.adam['beta2'] * self.adam['vt'].get(n, 0) + (1 - self.adam['beta2']) * dv ** 2
            mt_hat = self.adam['mt'][n] / (1 - (self.adam['beta1']) ** self.adam['t'])
            vt_hat = self.adam['vt'][n] / (1 - (self.adam['beta2']) ** self.adam['t'])
            weights[n] -= self.learning_rate * mt_hat / (np.sqrt(vt_hat) + self.adam['eps'])

    def gd_optimizer(self, parameters):
        grads = parameters['grads']
        weights = parameters['params']
        weights['U'] -= self.learning_rate * grads['U']
        weights['V'] -= self.learning_rate * grads['V']
        weights['W'] -= self.learning_rate * grads['W']

    def debug_bptt(self, input, prev_s_t):
        with open('../debug/input_backward_1',
                  'a') as f:
            f.write('---------------------------------------------\n')
            f.write('Inputs to backward:\n')
            f.write('Input: {}\n'.format(input))

            f.write('Previous hidden layer s:\n {}\n'.format(prev_s_t))
            f.write('U:\n {}\n'.format(self.U))
            f.write('W:\n {}\n'.format(self.W))
            f.write('V:\n {}\n'.format(self.V))

            f.write('End of inputs to backward\n')
            f.write('---------------------------------------------\n')

            f.close()

    def bptt_window(self, x, y):
        def init_s():
            s = np.zeros(self.hidden_dim)
            # s[len(x)] = 1
            return s

        assert len(x) == len(y)
        output = self.output_layer()

        dU = np.zeros(self.U.shape)
        dV = np.zeros(self.V.shape)
        dW = np.zeros(self.W.shape)

        T = len(x)
        self.orig_len = T - 1

        diff_s = np.zeros(self.hidden_dim)

        for t in range(0, T):
            input = self.generator(x[t], self.word_dim, self.orig_len)
            #input = self.generator(x[t], self.word_dim, self.orig_len-t)
            # generate the prev_s and prev_s_window for back propagation
            start = 0 if (t - self.bptt_truncate) < 0 else (t - self.bptt_truncate)
            end = t if t < self.bptt_truncate else self.bptt_truncate
            prev_s_t_win = init_s() if (t - self.bptt_truncate) <= 0 else layers_batch[
                0].s
            # prev s forward prop until the window size
            layers_batch = self.forward_propagation(x[start:t + 1], s=prev_s_t_win)
            prev_index = t - start - 1
            prev_s_t = layers_batch[prev_index].s if prev_index >= 0 else init_s()

            # appends the values to the file, comment this out if unnecessary
            # self.debug_bptt(input, prev_s_t)


            predict = output.predict(layers_batch[end].mulv)
            dmulv = output.derivative(layers_batch[end].mulv, y[t])

            # print('Predicted final mulv after activation: {}\n'.format(predict))

            dprev_s, dU_t, dW_t, dV_t = layers_batch[end].backward(input, prev_s_t, self.U, self.W, self.V, diff_s,
                                                                   dmulv)

            dmulv = np.zeros(self.output_dim)
            limit = min(t, self.bptt_truncate)
            for i in range(0, limit):
                input = self.generator(x[t - i - 1], self.word_dim, self.orig_len)
                #input = self.generator(x[t - i - 1], self.word_dim, self.orig_len - (t-i-1))
                prev_s_i = prev_s_t_win if i == limit - 1 else layers_batch[limit - i - 2].s

                rev = limit - i - 1 if t > 1 else 0
                dprev_s, dU_i, dW_i, dV_i = layers_batch[rev].backward(input, prev_s_i, self.U, self.W, self.V,
                                                                       dprev_s,
                                                                       dmulv)

                dU_t += dU_i
                dW_t += dW_i

            dV = dV_t
            dU = dU_t
            dW = dW_t
            self.optimizer(
                {'grads': {'V': dV, 'U': dU, 'W': dW}, 'params': {'U': self.U, 'V': self.V, 'W': self.W}},
            )

        return dU, dW, dV

    def bptt_seq(self, x, y):
        def init_s():
            s = np.zeros(self.hidden_dim)
            # s[len(x)] = 1
            return s

        assert len(x) == len(y)

        output = self.output_layer()
        layers = self.forward_propagation(x)
        dU = np.zeros(self.U.shape)
        dV = np.zeros(self.V.shape)
        dW = np.zeros(self.W.shape)

        T = len(x)
        self.orig_len = T - 1
        diff_s = np.zeros(self.hidden_dim)
        prev_s_t = init_s()

        for t in range(0, T):
            input = self.generator(x[t], self.word_dim, self.orig_len)
            #input = self.generator(x[t], self.word_dim, self.orig_len - t)
            # end of sequence
            dmulv = output.derivative(layers[t].mulv, y[t])
            #layers[t].forward(input, prev_s_t, self.U, self.W, self.V)
            predict = output.predict(layers[t].mulv)

            # print('Predicted final mulv after activation: {}\n'.format(predict))

            dprev_s, dU_t, dW_t, dV_t = layers[t].backward(input, prev_s_t, self.U, self.W, self.V, diff_s,
                                                           dmulv)
            prev_s_t = layers[t].s

            dmulv = np.zeros(self.output_dim)
            for i in range(t - 1, max(-1, t - self.bptt_truncate - 1), -1):
                input = self.generator(x[i], self.word_dim, self.orig_len)
                #input = self.generator(x[i], self.word_dim, self.orig_len-i)
                prev_s_i = init_s() if i == 0 else layers[i - 1].s

                dprev_s, dU_i, dW_i, dV_i = layers[i].backward(input, prev_s_i, self.U, self.W, self.V, dprev_s,
                                                               dmulv)
                dU_t += dU_i
                dW_t += dW_i

            dV += dV_t
            dU += dU_t
            dW += dW_t

        return dU, dW, dV

    def sgd_step(self, x, y):
        dU, dW, dV = self.bptt(x, y)
        for dparam in [dU, dW, dV]:
            np.clip(dparam, -5, 5, out=dparam)
        # TODO here the check is needed to determine window or end of sequence
        if self.update_type is not 'window':
            self.optimizer(
                {'grads': {'V': dV, 'U': dU, 'W': dW}, 'params': {'U': self.U, 'V': self.V, 'W': self.W}})

    def train(self, X, Y, nepoch=100, evaluate_loss_after=5):
        num_examples_seen = 0
        losses = []
        for epoch in range(nepoch):
            if epoch % evaluate_loss_after == 0:
                loss = self.calculate_total_loss(X, Y)
                losses.append((num_examples_seen, loss))

                print(
                    "Loss after num_examples_seen={0} epoch={1}: {2}".format(num_examples_seen, epoch, loss))
            for i in range(len(Y)):
                self.sgd_step(X[i], Y[i])
                num_examples_seen += 1
        return losses
