from core.activation import Tanh
from core.activation import Sigmoid
from core.gates import AddGate, MultiplyGate
import numpy as np

mulGate = MultiplyGate()
addGate = AddGate()
activation = Tanh()


class RNNLayer:
    def forward(self, x, prev_s, U, W, V):
        self.mulu = mulGate.forward(U, x)
        self.mulw = mulGate.forward(W, prev_s)
        self.add = addGate.forward(self.mulw, self.mulu)
        self.s = activation.forward(self.add)
        self.mulv = mulGate.forward(V, self.s)

    def backward(self, x, prev_s, U, W, V, diff_s, dmulv):
        # self.forward(x, prev_s, U, W, V)
        # predict = output.predict(self.mulv)
        # dmulv = output.derivative(self.mulv, y)
        dV, dsv = mulGate.backward(V, self.s, dmulv)
        ds = dsv + diff_s
        dadd = activation.backward(self.add, ds)
        dmulw, dmulu = addGate.backward(self.mulw, self.mulu, dadd)
        dW, dprev_s = mulGate.backward(W, prev_s, dmulw)
        dU, dx = mulGate.backward(U, x, dmulu)
        # with open('/Users/sruthiravi/DR/sigma-rnn/rnnexp/sigma/weights/backward', 'a') as f:
        #     f.write('---------------------------------------------')
        #     f.write('\nComputations inside backward for input: {}\n'.format(x))
        #     # f.write('Predicted final mulv after activation: {}\n'.format(predict))
        #     f.write('dmulv after forward propagation dmulv\n {}\n'.format(dmulv))
        #     f.write('Differtial of V - dV\n{}\n'.format(dV))
        #     f.write('Differtial of s - ds\n{}\n'.format(ds))
        #     f.write('Differtial of add - dadd\n{}\n'.format(dadd))
        #     f.write('Differtial of mulw - dmulw\n{}\n'.format(dmulw))
        #     f.write('Differtial of mulu - dmulu\n{}\n'.format(dmulu))
        #     f.write('Differtial of W- dW\n{}\n'.format(dW))
        #     f.write('Differtial of prev_s - dprev_s\n{}\n'.format(dprev_s))
        #     f.write('Differtial of U - dU\n{}\n'.format(dU))
        #     f.write('Differtial of x - dx\n{}\n'.format(dx))
        #     f.write('End of backward\n')
        #     f.write('---------------------------------------------')
        # f.close()
        return dprev_s, dU, dW, dV


class BiRNNLayer:
    # remove V its not used
    def forward(self, x, prev_s, U, W, V):
        self.mulu = mulGate.forward(U, x)
        self.mulw = mulGate.forward(W, prev_s)
        self.add = addGate.forward(self.mulw, self.mulu)
        self.s = activation.forward(self.add)

    def backward(self, x, prev_s, U, W, dsv, diff_s, dV):
        ds = dsv + diff_s
        dadd = activation.backward(self.add, ds)
        dmulw, dmulu = addGate.backward(self.mulw, self.mulu, dadd)
        dW, dprev_s = mulGate.backward(W, prev_s, dmulw)
        dU, dx = mulGate.backward(U, x, dmulu)
        return dprev_s, dU, dW, dV
