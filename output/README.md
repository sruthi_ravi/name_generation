### Config Description ###

config_4_best_caps = {                            
                            
    'optimizer': 'adam',
     'window_size': 2, # total window size is window_size +1 = 3
     'param_file': 'config_4_best_caps_w_250',
     'learning_rate': 1e-3,
     'update_type': 'window',
     'epoch': 250
}

* After many experiments, we see that the above config gives lower perplexity compared to other window and epochs settings. The length encoding experiments are conducted based on the configuration with varying epochs and window sizes. The hyper-parameter description for the same is below 

####config_4_best_caps_s_250_len.txt ####
    * caps - Upper case charcaters included in putput classes
    * s - End of sequence update
    * 250 epochs
    * len - The length of name given as the first character of input.
    * Window size - 3
    * Preplexity - 16

#### config_4_best_caps_s_500_len.txt ####
    * caps - Upper case charcaters included in output classes
    * s - End of sequence update
    * 500 epochs
    * len - The length of name given as the first character of input.
    * Window size - 3
    * Preplexity - 16.28
     
#### config_4_best_caps_s_500_len_bit.txt ####
    * caps - Upper case charcaters included in output classes
    * s - End of sequence update
    * 500 epochs
    * len_bit - The length of name encoded with every character of input.
    * Generation - Only the start character (#) is given.
    * Window size - 3
    * Preplexity - 11.63

#### config_4_best_caps_s_500_len_bit_1.txt ####
    * caps - Upper case charcaters included in output classes
    * s - End of sequence update
    * 500 epochs
    * len_bit - The length of name encoded with every character of input.
    * _1 => Generation - The start character (#) and first letter of the name (A-Z) is given.
    * Window size - 3
    * Preplexity - 11.63
    
#### config_4_best_caps_w5_250_len_bit.txt ####
    * caps - Upper case charcaters included in output classes
    * w - End of window update
    * 250 epochs
    * len_bit - The length of name encoded with every character of input.
    * Generation - Only the start character (#) is given.
    * Window size - 5
    * Preplexity - 8.65
#### config_4_best_caps_w5_500_len_bit.txt ####
    * caps - Upper case charcaters included in output classes
    * w - End of window update
    * 500 epochs
    * len_bit - The length of name encoded with every character of input.
    * Generation - Only the start character (#) is given.
    * Window size - 5
    * Preplexity - 9.13
      
#### config_4_best_caps_w5_500_len_bit_1.txt ####
    * caps - Upper case charcaters included in output classes
    * w - End of window update
    * 500 epochs
    * len_bit - The length of name encoded with every character of input.
    * _1 => Generation - The start character (#) and first letter of the name (A-Z) is given.
    * Window size - 5
    * Preplexity - 9.13 
    
#### config_4_best_caps_w7_250_len_bit.txt ####
    * caps - Upper case charcaters included in output classes
    * w - End of window update
    * 250 epochs
    * len_bit - The length of name encoded with every character of input.
    * Generation - Only the start character (#) is given.
      * Window size - 7
        * Preplexity - 9.32
      
#### config_4_best_caps_w_250_len.txt ####
    * caps - Upper case charcaters included in output classes
    * w - End of window update
    * 250 epochs
    * len - The length of name encoded with every character of input.
    * Window size - 3
       * Preplexity - 15.57
    
    
### config_4_best_caps_w_250_len_bit.txt ###
    ( Best model 8.07 perplexity)
    * caps - Upper case charcaters included in output classes
    * w - End of window update
    * 250 epochs
    * len_bit - The length of name encoded with every character of input.
    * Generation - Only the start character (#) is given.
    * Window size - 3
      * Preplexity - 8.07
      
### config_4_best_caps_w_250_len_bit_1.txt ### 
    ( Best model 8.07 perplexity)
    * caps - Upper case charcaters included in output classes
    * w - End of window update
    * 250 epochs
    * len_bit - The length of name encoded with every character of input.
    * _1 => Generation - The start character (#) and first letter of the name (A-Z) is given.
    * Window size - 3
        * Preplexity - 8.07
#### config_4_best_caps_w_250_len_decay.txt ####
    * caps - Upper case charcaters included in output classes
    * w - End of window update
    * 250 epochs
    * len_decay - The decreasing length of the name encoded with every character of input.
    * Window size - 3
    * Preplexity - 14.33
        
#### config_4_best_caps_w_500_len.txt ####
    * caps - Upper case charcaters included in output classes
    * w - End of window update
    * 500 epochs
    * len - The length of name encoded with every character of input.
     * Generation - Only the start character (#) is given.
     * Window size - 3
         * Preplexity - 14.98
     
#### config_4_best_caps_w_500_len_1.txt ####
    * caps - Upper case charcaters included in output classes
    * w - End of window update
    * 500 epochs
    * len - The length of name encoded with every character of input.
      * Generation - The start character (#) and first letter of the name (A-Z) is given.
     * Window size - 3
      * Preplexity - 14.98
          
#### config_4_best_caps_w_500_len_bit.txt ####
    * caps - Upper case charcaters included in output classes
    * w - End of window update
    * 500 epochs
    * len_bit - The length of name encoded with every character of input.
     * Generation - Only the start character (#) is given.
     * Window size - 3
         * Preplexity - 8.18
         
#### config_4_best_caps_w_500_len_bit_1.txt ####
    * caps - Upper case charcaters included in output classes
    * w - End of window update
    * 500 epochs
    * len_bit - The length of name encoded with every character of input.
      * Generation - The start character (#) and first letter of the name (A-Z) is given.
      * Window size - 3
    * Preplexity - 8.18
#### config_4_best_caps_w_500_len_decay.txt ####
    * caps - Upper case charcaters included in output classes
    * w - End of window update
    * 500 epochs
    * len_decay - The decreasing length of the name encoded with every character of input.
     * Generation - Only the start character (#) is given.
     * Window size - 3
            * Preplexity - 14.65
     
#### config_4_best_caps_w_500_len_decay_1.txt ####
    * caps - Upper case charcaters included in output classes
    * w - End of window update
    * 500 epochs
    * len_decay - The decreasing length of the name encoded with every character of input.
      * Generation - The start character (#) and first letter of the name (A-Z) is given.
      * Window size - 3
     * Preplexity - 14.65
             
#### config_4_best_caps_w_750_len_bit.txt ####
    * caps - Upper case charcaters included in output classes
    * w - End of window update
    * 750 epochs
    * len_bit - The length of name encoded with every character of input.
     * Generation - Only the start character (#) is given.
     * Window size - 3
    * Preplexity - 8.17
     
#### config_4_best_caps_w_750_len_bit_1.txt ####
    * caps - Upper case charcaters included in output classes
    * w - End of window update
    * 750 epochs
    * len_bit - The length of name encoded with every character of input.
      * Generation - The start character (#) and first letter of the name (A-Z) is given.
      * Window size - 3
     * Preplexity - 8.17
      
#### full_data_config_4_best_caps_w1_250_len_bit.txt ####
    * full data 5000 train samples 
    * caps - Upper case charcaters included in output classes
    * w - End of window update
    * 250 epochs
    * len_bit - The length of name encoded with every character of input.
     * Generation - Only the start character (#) is given.
     * Window size - 1
           * Preplexity - 7.83
     
#### full_data_config_4_best_caps_w3_250_len_bit.txt ####
    * full data 5000 train samples 
    * caps - Upper case charcaters included in output classes
    * w - End of window update
    * 250 epochs
    * len_bit - The length of name encoded with every character of input.
     * Generation - Only the start character (#) is given.
     * Window size - 3
           * Preplexity - 5.41
           
#### full_data_config_4_best_caps_w5_250_len_bit.txt #### 
    * full data 5000 train samples 
    * caps - Upper case charcaters included in output classes
    * w - End of window update
    * 250 epochs
    * len_bit - The length of name encoded with every character of input.
    * Generation - Only the start character (#) is given.
    * Window size - 5
           * Preplexity - 6.84
           
    
#### full_data_config_4_best_caps_w7_250_len_bit.txt ####
    * full data 5000 train samples 
    * caps - Upper case charcaters included in output classes
    * w - End of window update
    * 250 epochs
    * len_bit - The length of name encoded with every character of input.
    * Generation - Only the start character (#) is given.
    * Window size - 7
       * Preplexity - 7.64